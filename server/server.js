var express = require('express');
var logger = require('morgan');
var bodyParser = require('body-parser');
var methodOverride = require('method-override');
var cookieParser = require('cookie-parser');
var path = require('path');
var favicon = require('serve-favicon');

// connect database
var mongoose = require('mongoose');
mongoose.Promise = require('bluebird');

mongoose.connect(process.env.MONGO_URI || 'mongodb://dormouse:pier94@ds027495.mongolab.com:27495/dormouse', function (err) {
  if (err) { throw err; }
  console.info('Connection to the database was successfull');
});

//require('./players/player.population')().populate();


// Setup server
var app = express();

//app.use(require('connect-livereload')({port: 7777}));



// use middlewares
app.use(logger('dev'));
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

var expressValidator = require('express-validator');
app.use(expressValidator());

app.use(methodOverride());
app.use(cookieParser());

// serve static files
app.use(express.static(path.join(__dirname, '..')));
console.log((path.join(__dirname, '..'))); //stampo la directory dei file static
// define our API
    app.use('/api/players', require('./players'));


// catch 404
 app.get('/:url(api|bower_components|assets)/*', function (req, res) {
 res.status(404).send('Resource not found');
});

// serve always index.html
app.get('/*', function (req, res) {
  res.sendFile(path.join(__dirname, '..', 'index.html'));  
});

var port = process.env.PORT || 8080;


// Start server
app.listen(port, function () {
  console.log('Express server listening on %d', port);
});

