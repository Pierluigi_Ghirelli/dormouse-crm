var mongoose = require('mongoose');
var Player = require('./player.model');

module.exports = function () {
  // business logics
  
  /*
   * Query the players
   * 
   * @return {Array} players
   */
  var query = function (req, res) {
    Player.find().exec().then(function (players) {
      res.json(players);
    }).catch(function (err) {
      res.status(500).send(err);
    });
  };
  
  /*
   * Get a given item
   * 
   * @param {String} code
   * @return {Object} item
   */
  var get = function (req, res) {
    console.log(req.params.id);
    Player.findById(req.params.id).exec().then(function (player) {
      if (!player) { return res.status(404).send('Player not found'); }
      res.json(player);
    }).catch(function (err) {
      res.status(500).send(err);
    });
  };
  
  var save = function (req, res) {
    req.checkBody('name').notEmpty();
    var errors = req.validationErrors();
    if (errors) { return res.status(400).send(errors); }
    
    var id = req.body._id || mongoose.Types.ObjectId();
    Player.findByIdAndUpdate(id, req.body, {upsert: true}).then(function () {
      res.status(200).send();
    }).catch(function (err) {
      res.status(500).send(err);
    });
  };
  
  var remove = function (req, res) {
    Player.findByIdAndRemove(req.params.id).exec().then(function () {
      res.status(200).send();
    }).catch(function (err) {
      res.status(500).send(err);
    });
  };
  
  
  // public API
  return {
    get: get,
    query: query,
    remove: remove,
    save: save
  };
};