var Player = require('./player.model');

module.exports = function () {
  // business logics
  var players = require('./player.json');
  
  var populate = function (done) {
    done = done || function () {};
    Player.remove()
      .then(function(){
        console.info('Clean items collection...'); 
      //Prelevo le chiavi presenti nel oggetto players
        for (var i in players){    
            //console.log(i);
            //per ogni singola chiave/categoria prelevo tutti i players presenti
            for( var j of players[i]){
            //console.log("player:" + j); //stampo in console i players
            
            // per ogni players creo un instanza del modello 
            var newPlayer = new Player(j);
            newPlayer.save(); // e poi lo salvo nel db.
            };
        };
     })
      .then(function () {
        console.info('Items populated...');
        done();
      })
      .catch(function (err) {
        done(err);
      });
  };
  
  // public API
  return {
    populate: populate
  };
};