var express = require('express');
var controller = require('./players.controller.js')();
var router = express.Router();

router.get('/', controller.query);
router.get('/:id', controller.get);
router.delete('/:id', controller.remove);
router.post('/', controller.save);

module.exports = router;