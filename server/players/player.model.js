var mongoose = require('mongoose');
var Schema = mongoose.Schema;

// schema
var playerSchema = new Schema({
  name: {
    type: String,
    required: true
  },
  surname: {
    type: String,
    required: true
  },
  dataNasc: String,
  sesso: String,
  cittadinanza: String,
  codFisc: String,
  comuneAnag: String,
  provAnag: String,
  nazioneAnag: String,
  indirizzo: String,
  provDom: String,
  comuneDom: String,
  capDom: Number,
  email : String,
  tel : Number,
  cell1: Number,
  cell2: Number,
  categoria: String,
  image: String
});


var Player = mongoose.model('Player', playerSchema);

module.exports = Player;
