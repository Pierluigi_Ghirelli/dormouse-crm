var gulp = require('gulp');
var server = require('gulp-express');
var env = require('gulp-env');

gulp.task('default', function () {
  env({
    vars: {
      MONGO_URI: 'mongodb://dormouse:pier94@ds027495.mongolab.com:27495/dormouse'
    }
  });
  
  // Start the server at the beginning of the task 
  server.run(['server/server.js'], {}, 7777);

  // notify the client
  gulp.watch(['client/app/**/*.html'], server.notify);
  gulp.watch(['client/assets/**/*'], server.notify);
  gulp.watch(['client/app/**/*.js'], server.notify);

  // Restart the server when file changes 
  gulp.watch(['server/**/*.js'], [server.run]);
});