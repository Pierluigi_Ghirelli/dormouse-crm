angular.module('dormouse', ['ngMaterial','ui.router','ngResource'])
.config(function($mdThemingProvider) {
    $mdThemingProvider.theme('default')
      .primaryPalette('green')
      .accentPalette('grey')
      .warnPalette('blue')
      .backgroundPalette('grey');
  });


