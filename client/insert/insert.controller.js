angular.module("dormouse").controller("InsertController", function($scope,teamsService,$state,$mdToast){
   console.log("New Player RUN"); 
   
   $scope.categorie = teamsService.getCategorie();
   $scope.provincie = teamsService.getProv();
   
  
   $scope.save = function (player) {
    teamsService.addProfile(player).then(function () {
      $mdToast.show($mdToast.simple().content('Player salvato con successo.'));
      $state.go("list.categorie");
    }).catch(function (err) {
      $mdToast.show($mdToast.simple().content('Errore nel salvataggio dell\'player.'));
      console.error(err);
    });
  };
  
   
});