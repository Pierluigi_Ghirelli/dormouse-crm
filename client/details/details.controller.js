angular.module("dormouse").controller("DetailsController",function($scope,teamsService,profile,$state,$mdToast){
   
   console.log("Details Run!!"); 
   console.log(profile); //array di oggetti contenente un oggetto quello con l'id scelto 
  
   $scope.profile = profile; //prelevo il primo e uno oggetto nell array
   $scope.categorie = teamsService.getCategorie();
   $scope.provincie = teamsService.getProv();
   
   $scope.update = function(profile){
      teamsService.addProfile(profile);
      $state.go("list.categorie");
   };
   
   $scope.delete = function(profile){
         console.log(profile);
         teamsService.deleteProfile({id: profile._id}).then(function(){
         $state.go("list.categorie");  
         }).catch(function (err){
            console.error(err);
            $state.go("list.categorie");
            });
   };
   
   $mdToast.show($mdToast.simple().content('Profilo di ' + profile.name + ' ' + profile.surname +' caricato con successo'));

});



