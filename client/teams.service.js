angular.module("dormouse").factory("teamsService", function($resource){

var provincie = ('AG AL AN AO AQ AR AP AT AV BA BT BL BN BG BI BO BZ BS BR CA CL CB CI CE CT CZ CH CO CS CR KR CN EN FM FE FI FG FC FR GE GO GR IM IS SP LT LE LC LI LO LU MC MN MS MT VS ME MI MO MB NA NO NU OG OT OR PD PA PR PV PG PU PE PC PI PT PN PZ PO RG RA RC RE RI RN RO SA SS SV SI SR SO TA TE TR TO TP TN TV TS UD VA VE VB VC VR VV VI VT')
    .split(' ').map(function(provincia) {
        return {abbrev: provincia};
        });
        
var categorie = ["Scuola Calcio","Pulcini","Esordienti","Giovanissimi","Allievi","Juniores"];

var Teams = $resource('/api/players/:action/:id', {
    id: '@id',
    action: '@action'
  });
  
var query = function (callback) {
    callback = callback || angular.noop;
    return Teams.query(function (teams) {
      //console.log(teams);
      return callback(teams);
    }, function (err) {
      return callback(err);
    }).$promise;
  };
        

var getProfile = function(params,callback){
    callback = callback || angular.noop;
    console.log(params);
    return Teams.get(params, function (player) {
      return callback(player);
    }, function (err) {
        return callback(err);
        }).$promise;
};

var addProfile = function (profile, callback) {
    callback = callback || angular.noop;
    return Teams.save(profile, function () {
      return callback();
    }, function (err) {
      return callback(err);
    }).$promise;
  };

var deleteProfile = function (params, callback) {
    callback = callback || angular.noop;
    return Teams.remove(params, function () {
      return callback();
    }, function (err) {
      return callback(err);
    }).$promise;
  };

var getCategorie = function(){
 return categorie;
};

var getProv = function(){
 return provincie;
};


 // public API
 return {
     getProfile: getProfile,
     addProfile:addProfile,
     getProv:getProv,
     getCategorie:getCategorie,
     query:query,
     deleteProfile:deleteProfile,
     };
    
});


