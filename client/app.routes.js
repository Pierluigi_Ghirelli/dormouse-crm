angular.module('dormouse')
  .config(function ($stateProvider, $urlRouterProvider,$locationProvider) {
    $locationProvider.html5Mode(true).hashPrefix('!'); // toglie il canceletto dal url
    $urlRouterProvider.otherwise('/'); //qualsiasi altra root ti porta a home
    $stateProvider
      .state('home', {
        url: '/',
        templateUrl: "client/home/home.template.html",
        controller: "HomeController"
      })
      
      .state('list',{
        url:'/list',
        abstract: true,
        templateUrl: "client/list/list.template.html",
        
      })   
      
      .state('list.categorie', {
        url: '/categorie',
        templateUrl: "client/categorie/categorie.template.html",
        controller: "CategorieController"
      })
      
     .state('list.details',{
       url:'/details/:id',
       templateUrl: "client/details/details.template.html",
       controller: "DetailsController",
       resolve: {
         profile : function($stateParams,teamsService){
          console.log($stateParams.id);
          return teamsService.getProfile({id: $stateParams.id});
         }
       }
     })
     
     .state('insert',{
         url: '/insert',
         templateUrl: "client/insert/insert.template.html",
         controller: "InsertController"
     })
  });